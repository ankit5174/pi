import { toast } from 'react-toastify';
import ToastStatusEnum from '../constants/ToastStatusEnum';
import { _listAll } from './FirebaseUtils';

export function _insertIf(condition, ...elements) {
    return condition ? elements : [];
}

export const _toast = (toastData) => {
    let options = {
        position: toast.POSITION.TOP_CENTER,
        autoClose: 5000,
    };
    if (toastData.autoClose !== undefined && toastData.autoClose !== null) {
        options.autoClose = toastData.autoClose;
    }
    switch (toastData.type) {
        case ToastStatusEnum.Success:
            return toast.success(toastData.message, options);
        case ToastStatusEnum.Warning:
            return toast.warn(toastData.message, options);
        case ToastStatusEnum.Error:
            return toast.error(toastData.message, options);
        case ToastStatusEnum.Info:
            return toast.info(toastData.message, options);
        default:
            break;
    }
};

export const _isImageFile = (file) => {
    return file['type'].includes('image');
};

export const _calculateImageSize = (src) => {
    return new Promise((resolve, reject) => {
        const img = new Image();
        img.onload = function () {
            resolve({
                height: this.height,
                width: this.width,
            });
        };
        img.onerror = function () {
            reject();
        };
        img.src = src;
    });
};

export const _canvasToBlob = (canvas, type = 'image/jpeg', quality = 0.95) => {
    return new Promise((resolve, reject) => {
        if (canvas) {
            canvas.toBlob(function (blob) {
                resolve(blob);
            });
        } else {
            reject();
        }
    });
};

export const _to = (promise) => {
    return promise
        .then((response) => [response, undefined])
        .catch((error) => Promise.resolve([undefined, error.response]));
};

export const _getItemsFromPath = (path) => {
    return _listAll(path).then((response) => response.items);
};

export const _getThumbnailUrl = (thumbnailRef) => {
    return thumbnailRef.getDownloadURL();
};
