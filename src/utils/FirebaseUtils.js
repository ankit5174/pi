import * as firebase from 'firebase';

// Add the Firebase products that you want to use
import 'firebase/auth';
import 'firebase/firestore';

const firebaseConfig = {
    apiKey: 'AIzaSyAUZ_raOS2O5golc6s_uCDH111xOPKTB2c',
    authDomain: 'pi5174.firebaseapp.com',
    databaseURL: 'https://pi5174.firebaseio.com',
    projectId: 'pi5174',
    storageBucket: 'pi5174.appspot.com',
    messagingSenderId: '55037049471',
    appId: '1:55037049471:web:95126c92b962c01f75dd4a',
};

firebase.initializeApp(firebaseConfig);

const storage = firebase.storage();

export const _upload = (path, data) => {
    return storage.ref().child(path).put(data);
};

export const _listAll = (path) => {
    return storage.ref().child(path).listAll();
};
