import React, { useCallback, useEffect, useState } from 'react';
import Page from '../../components/common/Page';
import Toolbar from '../../components/common/Toolbar';
import { useDispatch, useSelector } from 'react-redux';
import { selectStrings } from '../../store/localization/localization-selector';
import Typography from '../../components/common/Typography';
import ThumbnailsAction from '../../store/thumbnails/thumbnails-action';
import { selectIsRequesting } from '../../store/request/request-selector';
import AppEnum from '../../constants/AppEnum';
import {
    _getItemsFromPath,
    _getThumbnailUrl,
    _toast,
} from '../../utils/CommonUtils';
import ToastStatusEnum from '../../constants/ToastStatusEnum';
import ThumbnailView from '../../components/ThumbnailView';
import style from './style.module.css';
import { useHistory } from 'react-router-dom';
import RoutesEnum from '../../constants/RoutesEnum';

function ThumbnailsPage() {
    const isLoading = useSelector((state) =>
        selectIsRequesting(state, [ThumbnailsAction.REQUEST_FETCH_THUMBNAILS])
    );
    const strings = useSelector((state) => selectStrings(state));

    const dispatch = useDispatch();
    const fetchThumbnails = useCallback(
        (path) => dispatch(ThumbnailsAction.fetchThumbnails(path)),
        [dispatch]
    );

    const history = useHistory();
    const [thumbsnails, updateThumbnails] = useState([]);
    const [processingThumbnails, toggleProcessingThumbnails] = useState(false);

    useEffect(() => {
        fetchThumbnails(AppEnum.STORAGE_REF_THUMBNAILS)
            .then(async (response) => {
                toggleProcessingThumbnails(true);
                let arrayOfThumbnails = await Promise.all(
                    response.prefixes.map(function (folderRef) {
                        return _getItemsFromPath(folderRef.location.path);
                    })
                );
                let arrayOfThumbnailUrl = await Promise.all(
                    arrayOfThumbnails.map(async (thumbnails) => {
                        return await Promise.all(
                            thumbnails.map((thumbnailRef) => {
                                return _getThumbnailUrl(thumbnailRef);
                            })
                        );
                    })
                );
                updateThumbnails(arrayOfThumbnailUrl);
                toggleProcessingThumbnails(false);
            })
            .catch((err) => {
                _toast({
                    type: ToastStatusEnum.Error,
                    message: strings.somethingWentWrong,
                    autoClose: 5000,
                });
            });
    }, [fetchThumbnails, strings.somethingWentWrong]);
    return (
        <Page
            isLoading={isLoading || processingThumbnails}
            toolbars={[
                <Toolbar styles={[style.toolbar]} key={1}>
                    <Typography styles={[style.title]} variant={'h6'}>
                        {strings.thumbnail}
                    </Typography>
                    <Typography
                        onClick={() => history.push(RoutesEnum.THUMB_UPLOAD)}
                        styles={[style.new]}
                        variant={'subttitle2'}>
                        {strings.new}
                    </Typography>
                </Toolbar>,
            ]}>
            {thumbsnails.length === 0 ? (
                <div className={style.text}>
                    <Typography>{strings.noThumbnails}</Typography>
                </div>
            ) : (
                <div>
                    {thumbsnails.map((thumbnail) => (
                        <ThumbnailView thumbnail={thumbnail} />
                    ))}
                </div>
            )}
        </Page>
    );
}

export default ThumbnailsPage;
