import React, { useState, useCallback } from 'react';
import Page from '../../components/common/Page';
import Toolbar from '../../components/common/Toolbar';
import { useSelector } from 'react-redux';
import { selectStrings } from '../../store/localization/localization-selector';
import Typography from '../../components/common/Typography';
import FileSelector from '../../components/FileSelector';
import style from './style.module.css';
import {
    _calculateImageSize,
    _canvasToBlob,
    _isImageFile,
    _toast,
} from '../../utils/CommonUtils';
import ToastStatusEnum from '../../constants/ToastStatusEnum';
import ThumbnailEditor from '../../components/ThumbnailEditor';
import { useDispatch } from 'react-redux';
import ThumbnailsAction from '../../store/thumbnails/thumbnails-action';
import { selectIsRequesting } from '../../store/request/request-selector';
import AppEnum from '../../constants/AppEnum';
import { useHistory } from 'react-router-dom';
import RoutesEnum from '../../constants/RoutesEnum';

const acceptedTypes = ['image/jpeg', 'image/png', 'image/jpg'];
const thumbnailSize = [
    {
        id: '755*450',
        height: 450,
        width: 755,
    },
    {
        id: '365*450',
        height: 450,
        width: 365,
    },
    {
        id: '365*212',
        height: 212,
        width: 365,
    },
    {
        id: '380*380',
        height: 380,
        width: 380,
    },
];

function ThumbnailsUploadPage() {
    const isLoading = useSelector((state) =>
        selectIsRequesting(state, [ThumbnailsAction.REQUEST_UPLOAD_IMAGE])
    );
    const strings = useSelector((state) => selectStrings(state));

    const dispatch = useDispatch();
    const uploadImage = useCallback(
        (path, data) => dispatch(ThumbnailsAction.uploadImage(path, data)),
        [dispatch]
    );

    const history = useHistory();
    const [file, updateFile] = useState(null);

    const isImage = (file) => {
        let flag = true;
        if (!(_isImageFile(file) && acceptedTypes.includes(file.type))) {
            _toast({
                type: ToastStatusEnum.Warning,
                message: `${strings.fileNotSupported(
                    acceptedTypes.map((type) => type.split('/')[1]).join(',  ')
                )}`,
                autoClose: 5000,
            });
            flag = false;
        }
        return flag;
    };

    const isHDImage = async (file) => {
        let flag = true;

        try {
            let imageSize = await _calculateImageSize(
                URL.createObjectURL(file)
            );
            if (
                imageSize &&
                !(imageSize.width === 1024 && imageSize.height === 1024)
            ) {
                _toast({
                    type: ToastStatusEnum.Warning,
                    message: strings.imageSize,
                    autoClose: 5000,
                });
                flag = false;
            }
        } catch (e) {
            flag = false;
        }

        return flag;
    };

    const onFileChange = async (file) => {
        if (isImage(file) && (await isHDImage(file))) {
            updateFile(file);
        }
    };

    const onSubmit = async (croppedCanvas) => {
        let blobs = await Promise.all(
            croppedCanvas.map(async (croppedCanvas) => {
                croppedCanvas.blob = await _canvasToBlob(
                    croppedCanvas.canvas,
                    file.type
                );
                return croppedCanvas;
            })
        );
        const folderPath = new Date().getTime();
        let responses = await Promise.all(
            blobs.map((data) => {
                return uploadImage(
                    `${AppEnum.STORAGE_REF_THUMBNAILS}/${folderPath}/${
                        file.name
                    }_${data.id}.${file.type.split('/')[1]}`,
                    data.blob
                );
            })
        );
        if (responses.every((response) => !!response)) {
            history.replace(RoutesEnum.THUMBNAILS);
            _toast({
                type: ToastStatusEnum.Success,
                message: strings.filesUploadedSuccess,
                autoClose: 5000,
            });
        }
    };

    return (
        <Page
            isLoading={isLoading}
            toolbars={[
                <Toolbar key={1}>
                    <Typography variant={'h6'}>{strings.thumbnail}</Typography>
                </Toolbar>,
            ]}>
            {!file && (
                <div className={[style.fileSelectorRoot].join(' ')}>
                    <div className={[style.fileSelectorContainer]}>
                        <FileSelector
                            acceptedTypes={acceptedTypes}
                            onChange={onFileChange}
                        />
                    </div>
                </div>
            )}
            {!!file && (
                <div className={[style.editorRoot].join(' ')}>
                    <ThumbnailEditor
                        file={URL.createObjectURL(file)}
                        onSubmit={onSubmit}
                        thumbnailSize={thumbnailSize}
                    />
                </div>
            )}
        </Page>
    );
}

export default ThumbnailsUploadPage;
