const supportedLanguage = ['en'];
export const selectLanguage = (local) => {
    return supportedLanguage.includes(local) ? strings[local] : strings['en'];
};

export const strings = {
    en: {
        somethingWentWrong: 'Something went wrong',
        thumbnail: 'Thumbnail',
        fileNotSupported: (acceptedTypes) =>
            `File not supported. Accepted File Types: ${acceptedTypes}`,
        imageSize: 'Image should be of size 1024*1024',
        chooseAspectRatio: 'Choose Aspect Ration',
        crop: 'Crop',
        croppedImages: 'Cropped Images',
        submit: 'Submit',
        filesUploadedSuccess: 'Files uploaded successfully',
        noThumbnails: 'No Thumbnails Available',
        new: 'New Thumbnail +',
    },
};
