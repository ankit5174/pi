import ActionUtils from '../../utils/ActionUtils';
import { _listAll, _upload } from '../../utils/FirebaseUtils';

export default class ThumbnailsAction {
    static REQUEST_UPLOAD_IMAGE = 'REQUEST_UPLOAD_IMAGE';

    static REQUEST_FETCH_THUMBNAILS = 'REQUEST_FETCH_THUMBNAILS';

    static uploadImage = (path, data) => {
        return async (dispatch) => {
            return await ActionUtils.createThunkEffect(
                dispatch,
                ThumbnailsAction.REQUEST_UPLOAD_IMAGE,
                _upload,
                path,
                data
            );
        };
    };

    static fetchThumbnails = (path, data) => {
        return async (dispatch) => {
            let [response] = await ActionUtils.createThunkEffect(
                dispatch,
                ThumbnailsAction.REQUEST_FETCH_THUMBNAILS,
                _listAll,
                path
            );
            return response;
        };
    };
}
