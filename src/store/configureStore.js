import { applyMiddleware, compose, createStore } from 'redux';
import thunk from 'redux-thunk';
import { selectLanguage } from '../assets/strings';

export let store = null;

export function getStore({ locale }) {
    const composeEnhancers =
        process.env.NODE_ENV === 'development'
            ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
            : compose;

    store = createStore(
        require('./rootReducer').default,
        {
            localization: {
                strings: selectLanguage(locale || 'en'),
            },
        },
        composeEnhancers(applyMiddleware(thunk))
    );
    return store;
}
