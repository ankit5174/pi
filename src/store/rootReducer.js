import { combineReducers } from 'redux';
import localizationReducer from './localization/localization-reducer';
import requestReducer from './request/request-reducer';

const appReducers = combineReducers({
    localization: localizationReducer,
    request: requestReducer,
});

const rootReducer = (state, action) => {
    switch (action.type) {
        default:
            return appReducers(state, action);
    }
};

export default rootReducer;
