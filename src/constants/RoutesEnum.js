const RoutesEnum = {
    THUMBNAILS: '/thumbnails',
    THUMB_UPLOAD: '/thumbnails/upload',
};

export default RoutesEnum;
