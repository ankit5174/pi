import React, { useRef } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import style from './style.module.css';

const FileSelector = (props) => {
    const inputRef = useRef();

    const {
        acceptedTypes = [],
        onChange,
        inputProps = {},
        ...restProps
    } = props;

    const onChangeAction = (event) => {
        let file = event.target.files && event.target.files[0];
        onChange(file);
    };

    return (
        <div className={[style.root].join(' ')} {...restProps}>
            <input
                className={[style.fileInput].join(' ')}
                ref={inputRef}
                type={'file'}
                value={''}
                onChange={onChangeAction}
                accept={acceptedTypes.join(',')}
                {...inputProps}
            />
            <div
                onClick={() => inputRef.current.click()}
                className={[style.fileUploadContainer]}>
                <FontAwesomeIcon
                    color="#cccccc"
                    icon={['fas', 'folder-plus']}
                    size={'6x'}
                />
            </div>
        </div>
    );
};

export default FileSelector;
