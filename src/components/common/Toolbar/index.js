import React from 'react';
import style from './style.module.css';

function Toolbar(props) {
    const { children, styles: toolbarStyles = [] } = props;
    const styles = [style.toolbar, ...toolbarStyles].join(' ');
    return <div className={styles}>{children}</div>;
}

export default Toolbar;
