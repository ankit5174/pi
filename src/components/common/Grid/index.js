import React from 'react';
import { _insertIf } from '../../../utils/CommonUtils';
import style from './style.module.css';

function Grid(props) {
    const {
        container = true,
        item = false,
        spacing,
        xs,
        children,
        className = [],
    } = props;

    return (
        <div
            className={[
                ..._insertIf(container && !item, style.gridContainer),
                ..._insertIf(item, style.item),
                ..._insertIf(container, style[`gridSpacingXS${spacing}`]),
                ..._insertIf(!!xs, style[`gridXS${xs}`]),
                ...className,
            ].join(' ')}>
            {children}
        </div>
    );
}

export default Grid;
