import { shallow } from '../../../enzyme';
import Image from './index';
import { expect } from 'chai';
import React from 'react';

describe('<Image />', () => {
    it('renders a image', () => {
        const image = shallow(
            <Image
                height={24}
                width={24}
                src={
                    'https://s3.ap-south-1.amazonaws.com/tpng-images/random/original/6be52f0b-56bf-4fdc-abd3-0bec9e58610c.png'
                }
            />
        );
        expect(image.find('img').length).equal(1);
    });
});
