import React, { useRef, useEffect } from 'react';
import style from './style.module.css';
import { _insertIf } from '../../../utils/CommonUtils';

const Image = (props) => {
    const imgRef = useRef();

    const {
        width,
        alt,
        height,
        src,
        styles: rootStyles = [],
        imgStyles = [],
        imgProps,
        getRef,
        ...restPrps
    } = props;
    const styles = [style.img, _insertIf(!!imgStyles, imgStyles)].join(' ');

    useEffect(() => {
        if (getRef) {
            getRef(imgRef);
        }
    }, []);

    return (
        <div
            className={[style.root, ...rootStyles].join(' ')}
            style={{
                width,
                height,
            }}
            {...restPrps}>
            <img
                ref={imgRef}
                {...imgProps}
                className={styles}
                alt={alt || 'Icon'}
                src={src}
            />
        </div>
    );
};

export default Image;
