import React from 'react';
import AppBar from '../AppBar';
import Loader from '../Loader';

const Page = (props) => {
    const { position, children, toolbars, isLoading = false } = props;

    return (
        <>
            <AppBar position={position || 'sticky'}>{toolbars}</AppBar>
            <main>
                <Loader isLoading={isLoading} />
                {children}
            </main>
        </>
    );
};

export default Page;
