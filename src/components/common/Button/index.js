import React from 'react';
import style from './style.module.css';
import { _insertIf } from '../../../utils/CommonUtils';
import Image from '../Image';
import SpinnerImage from '../../../assets/images/spinner.svg';

function Button(props) {
    const {
        children,
        fullWidth,
        size = 'medium',
        styles: buttonStyles = [],
        isLoading,
        ...restProps
    } = props;
    const styles = [
        style.button,
        style[size],
        style.colorprimary,
        ..._insertIf(fullWidth, style.fullWidth),
        ..._insertIf(isLoading, style.isLoading),
        ...buttonStyles,
    ].join(' ');
    return (
        <button className={styles} {...restProps}>
            {isLoading ? (
                <Image height={24} width={24} src={SpinnerImage} />
            ) : (
                children
            )}
        </button>
    );
}

export default Button;
