import React from 'react';
import { shallow } from '../../../enzyme';
import Button from './index';
import { expect } from 'chai';
import Image from '../Image';

describe('<Button />', () => {
    it('renders a button', () => {
        const button = shallow(<Button>Login</Button>);
        expect(button.text()).to.contain('Login');
    });
    it('should show a loader image', () => {
        const button = shallow(<Button isLoading={true}>Login</Button>);
        expect(button.find(Image)).to.have.lengthOf(1);
    });
});
