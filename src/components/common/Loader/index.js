import React from 'react';
import Backdrop from '../Backdrop';
import SpinnerImage from '../../../assets/images/spinner.svg';

const Loader = (props) => {
    const { isLoading } = props;

    return (
        <Backdrop open={isLoading}>
            <img alt={'spinner'} src={SpinnerImage} />
        </Backdrop>
    );
};

export default Loader;
