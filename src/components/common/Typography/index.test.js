import { shallow } from '../../../enzyme';
import { expect } from 'chai';
import React from 'react';
import Typography from './index';

describe('<Typography />', () => {
    it('renders a Typography', () => {
        const typography = shallow(<Typography />);
        expect(typography.find('span').length).equal(1);
    });
    it('renders a h5 Typography', () => {
        const typography = shallow(<Typography variant={'h5'} />);
        expect(typography.find('.h5').exists()).to.equal(true);
    });
    it('renders a h4 Typography', () => {
        const typography = shallow(<Typography variant={'h4'} />);
        expect(typography.find('.h4').exists()).to.equal(true);
    });
    it('renders a h6 Typography', () => {
        const typography = shallow(<Typography variant={'h6'} />);
        expect(typography.find('.h6').exists()).to.equal(true);
    });
    it('renders a subtitle1 Typography', () => {
        const typography = shallow(<Typography variant={'subtitle1'} />);
        expect(typography.find('.subtitle1').exists()).to.equal(true);
    });
});
