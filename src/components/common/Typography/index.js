import React from 'react';
import style from './style.module.css';

function Typography(props) {
    const {
        variant,
        children,
        styles: typographyStyles = [],
        ...restProps
    } = props;
    const styles = [style[variant], ...typographyStyles].join(' ');
    return (
        <span className={styles} {...restProps}>
            {children}
        </span>
    );
}

export default Typography;
