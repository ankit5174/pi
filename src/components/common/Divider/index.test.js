import { shallow } from '../../../enzyme';
import { expect } from 'chai';
import React from 'react';
import Divider from './index';

describe('<Divider />', () => {
    it('renders a Divider', () => {
        const divider = shallow(<Divider />);
        expect(divider.find('div').length).equal(1);
    });
    it('renders a vertical Divider', () => {
        const divider = shallow(<Divider variant={'vertical'} />);
        expect(divider.find('.vertical').exists()).to.equal(true);
    });
    it('renders a horizontal Divider', () => {
        const divider = shallow(<Divider variant={'horizontal'} />);
        expect(divider.find('.horizontal').exists()).to.equal(true);
    });
});
