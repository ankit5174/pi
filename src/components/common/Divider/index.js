import React from 'react';
import style from './style.module.css';

function Divider(props) {
    const { variant = 'horizontal', styles: dividerStyles = [] } = props;
    const styles = [style[variant], ...dividerStyles].join(' ');
    return <div className={styles} />;
}

export default Divider;
