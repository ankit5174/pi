import React from 'react';
import style from './style.module.css';

function Card(props) {
    const { children, styles: cardStyles = [] } = props;
    const styles = [style.card, ...cardStyles].join(' ');
    return <div className={styles}>{children}</div>;
}

export default Card;
