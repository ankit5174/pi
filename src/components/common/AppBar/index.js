import React from 'react';
import style from './style.module.css';
import { _insertIf } from '../../../utils/CommonUtils';

function AppBar(props) {
    const { position, children, elevation = true } = props;
    const styles = [
        style.appBar,
        style[position],
        _insertIf(elevation, style.shadow),
    ].join(' ');
    return <div className={styles}>{children}</div>;
}

export default AppBar;
