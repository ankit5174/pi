import React from 'react';
import style from './style.module.css';
import { _insertIf } from '../../../utils/CommonUtils';
import Typography from '../Typography';

function TextField(props) {
    const {
        type = 'text',
        placeholder,
        inputProps,
        fullWidth,
        onChange,
        value,
        error,
        helperText,
        styles = [],
    } = props;

    const inputStyles = [style.input].join(' ');
    const inputWrapperStyles = [
        style.inputWrapper,
        _insertIf(error, style.error),
        ...styles,
    ].join(' ');
    const rootStyles = [_insertIf(fullWidth, style.fullWidth)];
    return (
        <div className={rootStyles}>
            <div className={inputWrapperStyles}>
                <input
                    value={value}
                    onChange={onChange}
                    placeholder={placeholder}
                    type={type}
                    className={inputStyles}
                    {...inputProps}
                />
            </div>
            {!!helperText && (
                <Typography
                    variant={'caption'}
                    styles={[..._insertIf(error, style.helperError)]}>
                    {helperText}
                </Typography>
            )}
        </div>
    );
}

export default TextField;
