import { shallow } from '../../../enzyme';
import { expect } from 'chai';
import React from 'react';
import TextField from './index';
import Typography from '../Typography';

describe('<TextField />', () => {
    it('renders a textfield', () => {
        const textfield = shallow(<TextField />);
        expect(textfield.find('input').length).equal(1);
    });
    it('renders a textfield with a value', () => {
        const textfield = shallow(<TextField value={'Ankit'} />);
        expect(textfield.find('input').props().value).equal('Ankit');
    });
    it('renders a textfield with a placeholder', () => {
        const textfield = shallow(<TextField placeholder={'Ankit'} />);
        expect(textfield.find('input').props().placeholder).equal('Ankit');
    });
    it('renders a textfield with a helperText', () => {
        const textfield = shallow(<TextField helperText={'Ankit'} />);
        expect(textfield.find(Typography).props().children).equal('Ankit');
    });
    it('renders a textfield with a error border', () => {
        const textfield = shallow(<TextField error={true} />);
        expect(textfield.find('.error').exists()).to.equal(true);
    });
    it('renders a fullwidth textfield', () => {
        const textfield = shallow(<TextField fullWidth={true} />);
        expect(textfield.find('.fullWidth').exists()).to.equal(true);
    });
});
