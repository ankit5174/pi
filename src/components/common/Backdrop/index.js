import React from 'react';
import style from './style.module.css';
import { _insertIf } from '../../../utils/CommonUtils';

const Backdrop = (props) => {
    const { open, children } = props;

    return (
        <div
            className={[style.backdrop, ..._insertIf(open, style.open)].join(
                ' '
            )}>
            {children}
        </div>
    );
};

export default Backdrop;
