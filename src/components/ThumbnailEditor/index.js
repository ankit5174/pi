import React, { useEffect, useRef, useState } from 'react';
import { useSelector } from 'react-redux';
import { selectStrings } from '../../store/localization/localization-selector';
import Grid from '../common/Grid';
import TestImage from '../../assets/images/image.jpg';
import style from './style.module.css';
import Typography from '../common/Typography';
import Button from '../common/Button';

function ThumbnailEditor(props) {
    const strings = useSelector((state) => selectStrings(state));

    const imageRef = useRef();
    const cropRef = useRef();
    const [croppedCanvas, updateCroppedCanvas] = useState({});
    const [size, setSize] = useState(
        !!props.thumbnailSize ? props.thumbnailSize[0] : null
    );

    useEffect(() => {
        cropRef.current.onmousedown = function (event) {
            let dmx = event.clientX;
            let dmy = event.clientY;
            let eLeft = cropRef.current.offsetLeft;
            let eTop = cropRef.current.offsetTop;

            moveAt(event.clientX, event.clientY);

            function moveAt(pageX, pageY) {
                let left = pageX - (dmx - eLeft);
                let top = pageY - (dmy - eTop);
                let elementHeight = cropRef.current.offsetHeight;
                let elementWidth = cropRef.current.offsetWidth;
                if (
                    left > 0 &&
                    top > 0 &&
                    top + elementHeight < imageRef.current.offsetHeight &&
                    left + elementWidth < imageRef.current.offsetWidth
                ) {
                    cropRef.current.style.left = left + 'px';
                    cropRef.current.style.top = top + 'px';
                }
            }

            function onMouseMove(event) {
                moveAt(event.clientX, event.clientY);
            }

            document.addEventListener('mousemove', onMouseMove);

            const onMoveEnd = function () {
                document.removeEventListener('mousemove', onMouseMove);
                cropRef.current.onmouseup = null;
            };

            imageRef.current.onmouseout = onMoveEnd;

            cropRef.current.onmouseup = onMoveEnd;
        };

        cropRef.current.ondragstart = function () {
            return false;
        };
    }, []);

    const { file, thumbnailSize, onSubmit } = props;

    const onAspectChange = (e) => {
        setSize(
            thumbnailSize.find((size) => e.target.value + '' === size.id + '')
        );
    };

    const onCrop = () => {
        updateCroppedCanvas((croppedCanvas) => {
            let updatedCropCanvas = { ...croppedCanvas };
            let left = cropRef.current.offsetLeft - imageRef.current.offsetLeft;
            let top = cropRef.current.offsetTop - imageRef.current.offsetTop;
            let width = cropRef.current.offsetWidth;
            let height = cropRef.current.offsetHeight;
            let canvas = !!croppedCanvas[size.id]
                ? croppedCanvas[size.id].canvas
                : document.createElement('canvas');
            canvas.width = width;
            canvas.height = height;

            canvas
                .getContext('2d')
                .drawImage(
                    imageRef.current,
                    left,
                    top,
                    width,
                    height,
                    0,
                    0,
                    width,
                    height
                );
            updatedCropCanvas[size.id] = { canvas, left, top };
            return updatedCropCanvas;
        });
    };

    const onSubmitAction = () => {
        if (onSubmit) {
            onSubmit(
                Object.keys(croppedCanvas).map((id) => ({
                    id: id,
                    canvas: croppedCanvas[id].canvas,
                }))
            );
        }
    };

    return (
        <Grid container={true} spacing={2}>
            <Grid item={true} xs={9}>
                <div className={[style.imageRoot].join(' ')}>
                    <div className={[style.component].join(' ')}>
                        <div className={[style.imageContainer].join(' ')}>
                            <div
                                ref={cropRef}
                                style={{
                                    height: size.height,
                                    width: size.width,
                                    left: croppedCanvas[size.id]
                                        ? croppedCanvas[size.id].left
                                        : 0,
                                    top: croppedCanvas[size.id]
                                        ? croppedCanvas[size.id].top
                                        : 0,
                                }}
                                className={[style.crop].join(' ')}
                            />
                            <img
                                ref={imageRef}
                                src={file || TestImage}
                                alt={'test'}
                                draggable={'false'}
                            />
                        </div>
                    </div>
                </div>
            </Grid>
            <Grid item={true} xs={3}>
                <div className={[style.actionsContainer].join(' ')}>
                    <Typography variant={'h6'}>
                        {strings.chooseAspectRatio}
                    </Typography>
                    <div className={[style.mt8].join(' ')} />
                    <select
                        className={[style.select].join(' ')}
                        value={size.id}
                        onChange={onAspectChange}>
                        {thumbnailSize.map((size) => (
                            <option
                                key={size.id}
                                value={
                                    size.id
                                }>{`${size.width} * ${size.height}`}</option>
                        ))}
                    </select>
                    <div className={[style.mt12].join(' ')} />
                    <Button onClick={onCrop}>{strings.crop}</Button>
                    <div className={[style.mt12].join(' ')} />
                    {!!Object.keys(croppedCanvas).length && (
                        <>
                            <Typography variant={'h6'}>
                                {strings.croppedImages}
                            </Typography>
                            <div className={[style.mt12].join(' ')} />
                            {Object.keys(croppedCanvas).map((sizeId) => {
                                let size = thumbnailSize.find(
                                    (size) => sizeId + '' === size.id + ''
                                );
                                return (
                                    <Typography
                                        key={sizeId}
                                        variant={
                                            'caption'
                                        }>{`${size.width} * ${size.height}`}</Typography>
                                );
                            })}
                        </>
                    )}
                    {Object.keys(croppedCanvas).length ===
                        thumbnailSize.length && (
                        <>
                            <div className={[style.mt12].join(' ')} />
                            <Button onClick={onSubmitAction}>
                                {strings.submit}
                            </Button>
                        </>
                    )}
                </div>
            </Grid>
        </Grid>
    );
}

export default ThumbnailEditor;
