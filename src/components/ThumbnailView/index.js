import React from 'react';
import style from './style.module.css';

function ThumbnailView(props) {
    const { thumbnail } = props;

    return (
        <div className={[style.root].join(' ')}>
            {thumbnail.map((item, index) => (
                <figure className={[style[`galleryItem${index}`]].join(' ')}>
                    <img
                        alt={`gallerIte${index}`}
                        src={item}
                        className={[style.galleryImage].join(' ')}
                    />
                </figure>
            ))}
        </div>
    );
}

export default ThumbnailView;
