import React, { lazy, Suspense } from 'react';
import './App.css';
import { Provider } from 'react-redux';
import { getStore } from './store/configureStore';
import './assets/fontawesome';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Switch, Route, Redirect } from 'react-router-dom';
import RoutesEnum from './constants/RoutesEnum';
import Loader from './components/common/Loader';

const ThumbnailsUploadPage = lazy(() => import('./pages/ThumbnailsUploadPage'));
const ThumbnailsPage = lazy(() => import('./pages/ThumbnailsPage'));

let store = getStore({ locale: 'en' });

function App() {
    return (
        <Provider store={store}>
            <Suspense fallback={<Loader isLoading={true} />}>
                <ToastContainer />
                <Switch>
                    <Route
                        exact
                        path={RoutesEnum.THUMBNAILS}
                        render={(props) => <ThumbnailsPage {...props} />}
                    />
                    <Route
                        exact
                        path={RoutesEnum.THUMB_UPLOAD}
                        render={(props) => <ThumbnailsUploadPage {...props} />}
                    />
                    <Redirect to={RoutesEnum.THUMBNAILS} />
                </Switch>
            </Suspense>
        </Provider>
    );
}

export default App;
